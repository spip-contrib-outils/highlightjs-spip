/*
Language: SPIP
Author: bricebou <brice@reclic.dev>
Description: highlight.js support for SPIP CMS markup language
Category: markup
*/
export default function (hljs) {
  const regex = hljs.regex;

  const CADRES = regex.either(
    /\[/,
    /\]/,
    /\(/,
    /\)/,
    /{/,
    /}/
  );

  const OPERATORS = regex.either(
    /IN/,
    /=/,
    /!=/,
    />=/,
    />/,
    /</,
    /<=/
  );

  const FILTRES = regex.either(
    /\|[a-z0-9_]+/,
    /\|>/,
    /\|>=/,
    /\|<=/,
    /\|</,
    /\|==/
  );

  const COMMENT = {
    className: 'comment',
    variants: [
      {
        begin: /<!--/,
        end: /-->/,
        relevance: 0
      },
      {
        begin: /\[\(#REM\)/,
        end: /\]/,
      },
    ],
  };

  const BALISE = {
    className: 'tag',
    begin: regex.concat(
      /#[A-Z0-9_]+\*{0,2}/,
      regex.lookahead(
        regex.optional(/{[^}\)\]]+}/)
      )
    ),
    contains: [
      {
        className: 'attribute',
        begin: /{/,
        end: /}/,

        contains: [
          {
            className: 'punctuation',
            match: /,/,
          },
          {
            begin: regex.concat(/[a-z0-9_]+/, regex.lookahead(regex.concat(regex.optional(OPERATORS), regex.optional(/[[^}\)\]]+]/)))),
            contains: [
              {
                className: 'operator',
                match: OPERATORS,
              },
              {
                className: 'variable',
                match: /[^, }]+/,
              }
            ]
          }
        ]
      }
    ]
  };

  const FILTRE = {
    className: 'title.function',
    begin: regex.concat(
      FILTRES,
      regex.lookahead(regex.concat(
        regex.optional(OPERATORS),
        regex.optional(/{[a-zA-Z0-9_,'"-]+}/)
      ))
    ),
    contains: [
      {
        className: 'operator',
        match: OPERATORS
      },
      {
        className: 'variable',
        match: /{?[a-zA-Z0-9_,'"-]+}?/
      }
    ]
  };

  const BOUCLE = {
    className: 'tag',
    begin: regex.concat(
      /<\/?\/?BB?(OUCLE)?(\w*)?/,
      regex.lookahead(regex.concat(
        regex.optional(/\([a-zA-Z0-9_:-]+\)/),
        regex.optional(/({[^}]*})*/),
        />/
      ))
    ),
    end: />/,
    contains: [
      {
        className: 'property',
        match: /\([a-zA-Z0-9_:-]+\)/,
      },
      {
        className: 'attribute',
        begin: /{/,
        end: /}/,
        contains: [
          {
            match: regex.concat(
              / /,
              regex.lookahead(/[^}]*/)
            ),
            contains: [
              {
                className: 'operator',
                match: OPERATORS,
              },
              {
                className: 'variable',
                match: / [^}]*/,
              }
            ]
          }
        ]
      }
    ]
  };


  return {
    name: 'spip',
    contains: [
      COMMENT,
      FILTRE,
      BALISE,
      BOUCLE,
      {
        className: 'punctuation',
        match: CADRES,
        relevance: 0,
      }
    ]
  };
}