[(#REM)Test de commentaire]

[(#REM)Test de commentaire
sur plusieurs
lignes]

<!-- un commentaire classique -->

[(#INCLURE{fond=nom_du_squelette, argument, argument=xx})]

#BALISE
#BALISE{argument}
#BALISE{argument, argument, argument}
#BALISE*
#BALISE**
[(#BALISE)]
[(#BALISE{argument})]
[(#BALISE*{argument})]
[ avant (#BALISE) apres ]
[ avant (#BALISE{argument}|filtre) apres ]
[ avant (#BALISE{argument}|filtre{argument}|filtre) apres ]

<BOUCLEx(TABLE)>
    pour chaque élément
</BOUCLEx>

<B_test23>
    une seule fois avant
<BOUCLE_test23(source:test){extension IN jpg,png}{id_rubrique = 1}{id_rubrique != 1}{vu}>
    pour chaque élément
</BOUCLE_test23>
    une seule fois après
</B_test23>
    afficher ceci s'il n'y a pas de résultat
<//B_test23>

<B>
    une seule fois avant
<BOUCLE(TABLE){extension IN jpg,png}>
    pour chaque élément
</BOUCLE>
    une seule fois après
</B>
    afficher ceci s'il n'y a pas de résultat
<//B>


<BOUCLE_blog2(ARTICLES){par date}{inverse}>
    [<hr /><h1>(#DATE|annee|unique)</h1>]
        [<h2>(#DATE|affdate{'Y-m'}|unique|nom_mois)</h2>]
             <a href="#URL_ARTICLE">#TITRE</a><br />
</BOUCLE_blog2>